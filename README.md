# README #

These scripts prepare expression and genetic data to run eQTL association in *cis* and *trans* using matrix eQTL (http://www.bios.unc.edu/research/genomic_software/Matrix_eQTL/). 

### How do I get set up? ###

* Can either run the script as a standalone or within the pipeline.
    1. If run as a standalone must specify all path files. Example in /sc/orga/projects/epigenAD/coloc/coloc3/application/prepare_run_eQTL.R
    2. If run from the pipeline must create a folder with "data" (name of dataset) in /sc/orga/projects/epigenAD/coloc/data/, with subfolder /rawData/genotypes,  /rawData/covariates/, /rawData/expression/. Example in submit_apply_functions_QTL_pipeline_script_DLPFC.sh

* * *
## Standalone: prepare_run_eQTL.R 
* * *
## Pipeline: submit_apply_functions_QTL_pipeline_script_DLPFC.sh
Arguments used in apply_functions_QTL_pipeline_script.R to call two functions from functions_QTL_pipeline.R: 

1. prepare.matrixEQTL.pipeline.
2. run.eQTL.pipeline.

Example application in 'apply_functions_QTL_pipeline.R' 
must have in /rawData/ these sub-folders:

1. in folder called genotypes:
    1. genotype files (one per each chromosome) with one dosage value per sample.
    Genotype files also contains info for each SNP (map position) in first 6 columns of the genotype file.
    2. HEADER.tab file with headers if genotypes do not contain a header
2. in folder called expression:
    1. expression file with samples in columns and gene name in rows, and give basename in exprFileName
    2. HEADER.tab file with headers if expression data do not contain a header
3. covariates, normally with samples as rows and columns the covariates you want to consider (this will be rearranged with samples as columns).
    If genotype and expression have different sample names must have 2 columns "sampleIDexp", "sampleIDgen"

### FORMATS OF INPUT files:
1. covariate file with rows as individuals and variables want to adjust for in columns
2. expression must have only one column with ProbeID followed by sample columns
3. gene location file must have ProbeID and chr:start..stop
4. genotypes in dosage format with 1 value per sample and position of snps in first 6 columns of genotype file


### OUTPUTS for each chromosome/region:
1. SNP_file
2. snps_location_file
3. expression_file
4. gene_location_file
5. covariates_file

*Note: If geneInfo/covariates files have already been created it does not overwrite these

## example modifications to prepare data:
      # cov = read.delim("/sc/orga/projects/epigenAD/coloc/data/CMC/rawData/covariates/DLPFC.ensembl.KNOWN_AND_SVA.ALL.SAMPLE_COVARIATES.tsv", header=T)
      # cov$AFF <- as.numeric(cov$Dx == "AFF") # don't do this for DLPFC.eRNA
      # cov$SCZ <- as.numeric(cov$Dx == "SCZ")
      # cov= cov[,c(1,3,175,176,150:154)] # if DLPFC
      ## cov = cov[,c(118,3,175,176,150:154)] # if ACC
      ## cov = cov[,c(1,3,175,150:154)] # if DLPFC.eRNA
      # names(cov)[1]="sampleIDexp" 
      # names(cov)[2]="sampleIDgen"
      # write.table(cov, file=paste(base.folder, '/data/', dataset, '/rawData/covariates/covariates_', condition, '.tab', sep = ''), sep="\t", row.names = FALSE, quote = FALSE, col.names = TRUE)


      # expression$Gene = as.character(lapply(strsplit(as.character(expression$Gene),"\302\247", fixed=TRUE), "[", 1))
      # names(expression)[1]="id"
      # write.table(expression, file="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/eRNAgene.txt", row.names = FALSE, quote = FALSE, col.names = TRUE)


      # if keep only a subset, must provide this in character vector
      # remove individuals and snps listed in the following files
      # goodSNPSFile = "/sc/orga/projects/epigenAD/coloc/data/CMC/support/CMC_Caucasian_good_SNPs.txt" 
      # goodIndividualsFile = "/sc/orga/projects/epigenAD/coloc/data/CMC/support/CMC_Caucasian_good_individuals_Control+SCZ+BP" 
      # goodSNPS = read.table(goodSNPSFile, header=T,stringsAsFactors=FALSE) 
      # goodSNPS=goodSNPS$SNP; subset.snps = goodSNPS
      # goodIndividuals = read.table(goodIndividualsFile, header=F, stringsAsFactors=FALSE) 
      # subset.individuals = as.character(goodIndividuals[,1])