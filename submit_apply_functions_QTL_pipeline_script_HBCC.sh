# bsub -P acc_roussp01a -q expressalloc -n 8 -W 60 -R "span[hosts=1]" -m mothra -Ip /bin/bash /hpc/users/giambc02/cluster/submission/permutations/script_TRUE_permutations.sh
######## important to set this before submission
# interactive specifis whether the scripts are submitted interactively in purcell1
# ps -eaf|grep giambc02
interactive=true
#interactive=false

# ds=(21)
ds=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22)
project="submitEQTL"
outputfolder='/hpc/users/giambc02/cluster/'
########
shopt -s expand_aliases
source ~/.bashrc
#export PATH=${PATH}:/share/apps/R-3.0.2/bin
script_output_directory=$outputfolder"submission/"$project
[[ -d "$script_output_directory" ]] && rm -r "$script_output_directory"
mkdir $script_output_directory
cd $script_output_directory
for chromosome in ${ds[@]}
do
Rscriptname='/hpc/users/giambc02/scripts/GENERAL/apply_functions_QTL_pipeline_script.R'
scriptname="script_chr"$chromosome"_submitEQTL.sh"
# cp $repoFolder"/granges_annotations6.R" $Rscriptname
#rOutputFileName="biom_chr"$chr".RData"
#rInput='oFile <- '"'$rOutputFileName'"'; project='"'$project'"''
#rOutputFileName="/sc/orga/projects/roussp01a/Claudia_TMP/data2/"
#rInput='oFile <- project='"'$project'"''; '"'$rOutputFileName'"'
#echo $rInput | cat - $Rscriptname > temp && mv temp $Rscriptname
if ! $interactive ; then 
echo '
#!/bin/bash
          #BSUB -J '$project$chromosome'
          #BSUB -q alloc
          #BSUB -P acc_roussp01a
          #BSUB -R span[hosts=1]
          #BSUB -R rusage[mem=24000]
          #BSUB -W 24:00 
          #BSUB -m bode
          #BSUB -L /bin/bash
          #BSUB -oo '$outputfolder'out/'$project$chromosome'
          #BSUB -eo '$outputfolder'error/'$project$chromosome > $scriptname
echo 'module unload R' >> $scriptname
echo 'module load R/3.2.2' >> $scriptname
echo 'R --vanilla < '$Rscriptname' --args dataset="HBCC" condition="DLPFC" chromosome='$chromosome' exprFileName="DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.GE.WITHOUT_PRIMARY.RESIDUALS.tsv" subset.snps.file="NULL" subset.individuals.file="/sc/orga/projects/epigenAD/coloc/data/HBCC/support/HBCC_Caucasian_individuals" geneFile="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt" base.folder="/sc/orga/projects/epigenAD/coloc" trans=FALSE cisDist=1e6' >> $scriptname
echo "Running" $Rscriptname "on cluster as" $scriptname
bsub < $scriptname
fi

if $interactive ; then 
echo 'module unload R' >> $scriptname
echo 'module load R/3.2.2' >> $scriptname
echo 'R --vanilla < '$Rscriptname' --args dataset="HBCC" condition="DLPFC" chromosome='$chromosome' exprFileName="DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.GE.WITHOUT_PRIMARY.RESIDUALS.tsv" subset.snps.file="NULL" subset.individuals.file="/sc/orga/projects/epigenAD/coloc/data/HBCC/support/HBCC_Caucasian_individuals" geneFile="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt" base.folder="/sc/orga/projects/epigenAD/coloc" trans=FALSE cisDist=1e6' >> $scriptname
nohup bash $scriptname > $outputfolder'out/'$project$chromosome 2> $outputfolder'error/'$project$chromosome &
fi

done
# --args dataset="CMC" condition="DLPFC" chromosome=chromosome exprFileName="DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv" subset.snps.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab" subset.individuals.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab" geneFile="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt" base.folder="/sc/orga/projects/epigenAD/coloc" trans=TRUE cisDist=1e6


# make sure all genotype files created have the same exact sample IDs!!! Otherwise matching with expression will be messed up!
# for chr in {1..22}; do  awk '{print NF; exit}' /sc/orga/projects/epigenAD/coloc/data/CMC/genotypes/chr$chr; done
