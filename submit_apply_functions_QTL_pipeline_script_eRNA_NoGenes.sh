# Pre-process
# save in /sc/orga/projects/epigenAD/coloc/data/CMC/reference/eRNA_NoGenes.txt
# Then there are the coordinates "chr" "start",and "end", with the same start and end coordinates. These are the most accurate to use for the eQTL analyses, as they are accurately centered on the actual enhancers. They should then all be resized to 3kb windows (subtracting 1.5kb from start and adding 1.5kb to the end) in addition to the window size you use.

# load("/sc/orga/projects/roussp01a/mads_atacseq/data/eRNA/fantom-5-p2-analyzed/quantification/aggregated-simple.RData")
# geneInfo = peakInfo[,c("id", "chr", "start", "end")]
# geneInfo$start = geneInfo$start-1500
# geneInfo$end = geneInfo$end+1500
# names(geneInfo) = c("geneid", "chr", "s1", "s2")
# write.table(geneInfo, file="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/eRNA_NoGenes.txt", row.names = FALSE, quote = FALSE, col.names = TRUE)


### Run with cis window: 1e6, 2000, 40000
######## important to set this before submission
# interactive specifis whether the scripts are submitted interactively in purcell1
# ps -eaf|grep giambc02
interactive=true
#interactive=false

# ds=(21)
ds=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X)
project="submitEQTLeRNANoGenes"
outputfolder='/hpc/users/giambc02/cluster/'
########
shopt -s expand_aliases
source ~/.bashrc
#export PATH=${PATH}:/share/apps/R-3.0.2/bin
script_output_directory=$outputfolder"submission/"$project
[[ -d "$script_output_directory" ]] && rm -r "$script_output_directory"
mkdir $script_output_directory
cd $script_output_directory
for chromosome in ${ds[@]}
do
Rscriptname='/hpc/users/giambc02/scripts/GENERAL/apply_functions_QTL_pipeline_script.R'
scriptname="script_chr"$chromosome"_"$project".sh"
# cp $repoFolder"/granges_annotations6.R" $Rscriptname
#rOutputFileName="biom_chr"$chr".RData"
#rInput='oFile <- '"'$rOutputFileName'"'; project='"'$project'"''
#rOutputFileName="/sc/orga/projects/roussp01a/Claudia_TMP/data2/"
#rInput='oFile <- project='"'$project'"''; '"'$rOutputFileName'"'
#echo $rInput | cat - $Rscriptname > temp && mv temp $Rscriptname
if ! $interactive ; then 
echo '
#!/bin/bash
          #BSUB -J '$project$chromosome'
          #BSUB -q alloc
          #BSUB -P acc_roussp01b
          #BSUB -R span[hosts=1]
          #BSUB -R rusage[mem=24000]
          #BSUB -W 24:00 
          #BSUB -m bode
          #BSUB -L /bin/bash
          #BSUB -oo '$outputfolder'out/'$project$chromosome'
          #BSUB -eo '$outputfolder'error/'$project$chromosome > $scriptname
echo 'module unload R' >> $scriptname
echo 'module load R/3.2.2' >> $scriptname
echo 'R --vanilla < '$Rscriptname' --args dataset="CMC" condition="eRNA_NoGenes" chromosome='$chromosome' exprFileName="DLPFC.eRNA.NoGenes" subset.snps.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab" subset.individuals.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab" geneFile="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/eRNA_NoGenes.txt" base.folder="/sc/orga/projects/epigenAD/coloc" trans=FALSE cisDist=40000' >> $scriptname 

echo "Running" $Rscriptname "on cluster as" $scriptname
bsub < $scriptname
fi

if $interactive ; then 
echo 'module unload R' >> $scriptname
echo 'module load R/3.2.2' >> $scriptname
echo 'R --vanilla < '$Rscriptname' --args dataset="CMC" condition="eRNA_NoGenes" chromosome='$chromosome' exprFileName="DLPFC.eRNA.NoGenes" subset.snps.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab" subset.individuals.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab" geneFile="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/eRNA_NoGenes.txt" base.folder="/sc/orga/projects/epigenAD/coloc" trans=FALSE cisDist=40000' >> $scriptname 
nohup bash $scriptname > $outputfolder'out/'$project$chromosome 2> $outputfolder'error/'$project$chromosome &
fi

done
# --args dataset="CMC" condition="DLPFC" chromosome=chromosome exprFileName="DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv" subset.snps.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab" subset.individuals.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab" geneFile="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt" base.folder="/sc/orga/projects/epigenAD/coloc" trans=TRUE cisDist=1e6

# make sure all genotype files created have the same exact sample IDs!!! Otherwise matching with expression will be messed up!
# for chr in {1..22}; do  awk '{print NF; exit}' /sc/orga/projects/epigenAD/coloc/data/CMC/genotypes/chr$chr; done

