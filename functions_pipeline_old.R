#########################################################
# This function takes in:
# nameList -	Character vector with the names of the first layer of the datasets (e.g. Enhancers, TF, BroadPeaks...)
 ## If nameList not specified the function will take all the annotations in an RData file
# RDataFile -	.RData file path with the annotation info 
 ## Can take the complete .RData file for REMC, but keep in mind
 ## system.time(overlapGR(RDataFile="/sc/orga/projects/roussp01a/Claudia_TMP/data/REMC.BroadPeaks.Rdata", snpDf=region)) # This takes 4.6 mins
 ## system.time(overlapGR(nameList="BroadPeaks", RDataFile="/sc/orga/projects/roussp01a/Claudia_TMP/data/REMC.Rdata", snpDf=region)) # This takes 11.51 mins!
# snpDf  -	Data frame with headers "CHR", "START", "STOP", "SNP", already loaded in R
 ## ex.
 ## t=overlapGR(RDataFile="/sc/orga/projects/roussp01a/Claudia_TMP/data/SE.Rdata", snpDf=region)
 ## t1=overlapGR(nameList=c("coreHMM","BroadPeaks"), RDataFile="/sc/orga/projects/roussp01a/Claudia_TMP/data/REMC.Rdata", snpDf=region)
## For REMC (auxHMM, coreHMM) and ENCODE (chromHMM) SNPs are overlapped with each row of the Genomic Range object

overlapGR <- function(nameList=NA, RDataFile, snpDf) {
         suppressPackageStartupMessages({
         require(GenomicRanges);
         });
         
         RDataName=gsub(".Rdata", "", basename(RDataFile))
         message("Loading ", RDataFile, " ...")
         listANNOT=get(load(RDataFile))

         snps=GRanges(seqnames=snpDf$CHR,
               ranges=IRanges(start=snpDf$START,
                              end=snpDf$STOP, names=snpDf$SNP),
               name=snpDf$SNP) # names of regions are the index SNPs

         ANNOT=data.frame(row.names=snpDf$SNP)
         if (all(is.na(nameList))) (nameList=names(listANNOT))
         for (name in nameList) {
            if (!is.list(listANNOT[[name]]) & !(any(grepl("chromHMM|auxHMM|coreHMM", nameList)==TRUE) | any(grepl("chromHMM|auxHMM|coreHMM", RDataName)==TRUE))) { # as.character(class(listANNOT[[name]]))=="GRanges" only for REMC.auxHMM.Rdata and REMC.core.Rdata
              OBJ <- as.data.frame(do.call(cbind, lapply(listANNOT[[name]], function(x) countOverlaps(snps, x, type=c("within"))), quote=TRUE))
              colnames(OBJ) = paste(RDataName, name, names(OBJ), sep=".")
              ANNOT=cbind.data.frame(ANNOT, OBJ)
              } 
            if (is.list(listANNOT[[name]]) &  !(any(grepl("chromHMM|auxHMM|coreHMM", nameList)==TRUE) | any(grepl("chromHMM|auxHMM|coreHMM", RDataName)==TRUE))) { # This is the case for REMC Peaks
              message(name, " contains additional list layers") 
              for (i in names(listANNOT[[name]])) {
                   if (!is.list(listANNOT[[name]][[i]])) {
                    OBJ <- as.data.frame(do.call(cbind, lapply(listANNOT[[name]][[i]], function(x) countOverlaps(snps, x, type=c("within"))), quote=TRUE))
                    colnames(OBJ) = paste(RDataName, name, i, names(OBJ), sep=".")
                    ANNOT=cbind.data.frame(ANNOT, OBJ)
                    }
                   }
               }
            if (any(grepl("chromHMM|auxHMM|coreHMM", nameList)==TRUE) | any(grepl("chromHMM|auxHMM|coreHMM", RDataName)==TRUE)) {
              ##if (any(grepl("auxHMM|coreHMM", nameList)==TRUE) | any(grepl("auxHMM|coreHMM", RDataName)==TRUE)) {
              if (any(grepl("auxHMM|coreHMM", RDataName)==TRUE)) {
                list_state = split(listANNOT[[name]], listANNOT[[name]]$id)
                OBJ <- as.data.frame(do.call(cbind, lapply(list_state, function(x) countOverlaps(snps, x, type=c("within"))), quote=TRUE))
                colnames(OBJ) = paste(RDataName, name, names(OBJ), sep=".")
                ANNOT=cbind.data.frame(ANNOT, OBJ)
                }
                ##id= mcols(listANNOT[[name]])$id
                ##o <- findOverlaps(snps, unlist(listANNOT[[name]]), type='within')
                ##OBJ <- matrix(0, nrow=length(snps), ncol=length(unlist(listANNOT[[name]])))
                ##OBJ[cbind(queryHits(o), subjectHits(o))] <- 1
                ##rownames(OBJ)=snpDf$SNP
                ##colnames(OBJ)=paste(RDataName, name, id, sep="$")
                ##OBJ=as.data.frame(OBJ)
                ##ANNOT=cbind.data.frame(ANNOT, OBJ)
                
               # for (i in 1:length(id)) {
               #    OBJ=as.data.frame(countOverlaps(snps, listANNOT[[name]][i,], type=c("within")))
               #    names(OBJ)=paste(RDataName, name, id[i], sep="$")
               #    ANNOT=cbind.data.frame(ANNOT, OBJ)
               # }

               ##if (any(grepl("chromHMM", nameList)==TRUE) | any(grepl("chromHMM", RDataName)==TRUE)  | RDataName=="REMC")  {
               if (RDataName=="REMC" & any(grepl("coreHMM", nameList)==TRUE) | (RDataName=="ENCODE" & any(grepl("chromHMM", nameList)==TRUE))) {
                 for (i in names(listANNOT[[name]])) {

                  list_state = split(listANNOT[[name]][[i]], listANNOT[[name]][[i]]$id)
                  OBJ <- as.data.frame(do.call(cbind, lapply(list_state, function(x) countOverlaps(snps, x, type=c("within"))), quote=TRUE))
                  colnames(OBJ) = paste(RDataName, name, i, names(OBJ), sep=".")
                  ANNOT=cbind.data.frame(ANNOT, OBJ)
                  }
                }

                   ##id= mcols(listANNOT[[name]][[i]])$id
                   ##o <- findOverlaps(snps, unlist(listANNOT[[name]][[i]]), type='within')
                   ##OBJ <- matrix(0, nrow=length(snps), ncol=length(unlist(listANNOT[[name]][[i]])))
                   ##OBJ[cbind(queryHits(o), subjectHits(o))] <- 1 
                   ##rownames(OBJ)=snpDf$SNP
                   ##colnames(OBJ)=paste(RDataName, name, i, id, sep="$")
                   ##OBJ=as.data.frame(OBJ)
                   ##ANNOT=cbind.data.frame(ANNOT, OBJ)
          
                   #for (j in 1:length(id)) {
                   # OBJ=as.data.frame(countOverlaps(snps, listANNOT[[name]][[i]][j],  type=c("within")))
                   # names(OBJ)=paste(RDataName, name, id[j], sep="$")
                   # ANNOT=cbind.data.frame(ANNOT, OBJ)
                   #}
            }
          if(max(ANNOT)>1) stop("Matrix is not binary")
          }
          return(ANNOT)
          }

#            if (!is.list(listANNOT[[name]]) & (RDataName %in% c("REMC.auxHMM", "REMC.coreHMM"))) { # This includes no lists and directly Genomic Ranges 
#              OBJ <- as.data.frame(do.call(cbind, lapply(listANNOT[name], function(x) countOverlaps(snps, x, type=c("within"))), quote=TRUE))
#              colnames(OBJ) = paste(RDataName, name, names(OBJ), sep="$")
#              ANNOT=cbind.data.frame(ANNOT, OBJ)



overlapGR2 <- function(nameList=NA, RDataFile, snpDf) {
         suppressPackageStartupMessages({
         require(GenomicRanges);
         });

         RDataName=gsub(".Rdata", "", basename(RDataFile))
         message("Loading ", RDataFile, " ...")
         listANNOT=get(load(RDataFile))

         snps=GRanges(seqnames=snpDf$CHR,
               ranges=IRanges(start=snpDf$START,
                              end=snpDf$STOP, names=snpDf$SNP),
               name=snpDf$SNP) # names of regions are the index SNPs

         ANNOT=data.frame(row.names=snpDf$SNP)
         if (all(is.na(nameList))) (nameList=names(listANNOT))
         for (name in nameList) {
            if (!is.list(listANNOT[[name]]) & (class(listANNOT[[name]])!="GRanges")) { # as.character(class(listANNOT[[name]]))=="GRanges" only for REMC.auxHMM.Rdata and REMC.core.Rdata
              OBJ <- as.data.frame(do.call(cbind, lapply(listANNOT[[name]], function(x) countOverlaps(snps, x, type=c("within"))), quote=TRUE))
              colnames(OBJ) = paste(RDataName, name, names(OBJ), sep="$")
              ANNOT=cbind.data.frame(ANNOT, OBJ)
              }
            if (is.list(listANNOT[[name]]) & (class(listANNOT[[name]])!="GRanges")) { # This is the case for REMC Peaks
              message(name, " contains additional list layers")
              for (i in names(listANNOT[[name]])) {
                   if (!is.list(listANNOT[[name]][[i]])) {
                    OBJ <- as.data.frame(do.call(cbind, lapply(listANNOT[[name]][[i]], function(x) countOverlaps(snps, x, type=c("within"))), quote=TRUE))
                    colnames(OBJ) = paste(RDataName, name, i, names(OBJ), sep="$")
                    ANNOT=cbind.data.frame(ANNOT, OBJ)
                    }
                   }
               }
            if (!is.list(listANNOT[[name]]) & (class(listANNOT[[name]])=="GRanges")) { # This includes no lists and directly Genomic Ranges  
              id= mcols(listANNOT[[name]])$id
              for (i in 1:length(id)) {
                   OBJ=as.data.frame(countOverlaps(snps, listANNOT[[name]][i,], type=c("within")))
                   names(OBJ)=paste(RDataName, name, id[i], sep="$")
                   ANNOT=cbind.data.frame(ANNOT, OBJ)
                   }
               }
          } 
          return(ANNOT)
}
