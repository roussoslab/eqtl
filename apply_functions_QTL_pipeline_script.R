## Collect arguments
args <- commandArgs(TRUE)

## Parse arguments (we expect the form --arg=value)
parseArgs <- function(x) strsplit(sub("^--", "", x), "=")
argsDF <- as.data.frame(do.call("rbind", parseArgs(args)))
argsL <- as.list(as.character(argsDF$V2))
names(argsL) <- argsDF$V1

print(args)
print(class(args))
print(length(args))

namesAll = as.character(lapply(strsplit(as.character(args), "=", fixed=TRUE), "[", 1))
valuesAll= as.character(lapply(strsplit(as.character(args), "=", fixed=TRUE), "[", 2))
for (i in 1:length(args)) {assign(namesAll[i], valuesAll[[i]])}

# Change the classes (for now all character) where needed for this function
# args=c("dataset=CMC", "condition=DLPFC", "chromosome=21", "exprFileName=DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv", "subset.snps.file=/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab", "subset.individuals.file=/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab", "geneFile=/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt", "base.folder=/sc/orga/projects/epigenAD/coloc", "trans=TRUE","cisDist=1e6", "use.covariates.if.available = TRUE")
# namesAll = as.character(lapply(strsplit(as.character(args), "=", fixed=TRUE), "[", 1))
# valuesAll= as.character(lapply(strsplit(as.character(args), "=", fixed=TRUE), "[", 2))
# for (i in 1:length(args)) {assign(namesAll[i], valuesAll[[i]])}

# args=c("dataset=CMC", "condition=ACC", "chromosome=21", "exprFileName=ACC.ensembl.KNOWN_AND_SVA.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv", "subset.snps.file=/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab", "subset.individuals.file=/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab", "geneFile=/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt", "base.folder=/sc/orga/projects/epigenAD/coloc", "trans=TRUE","cisDist=1e6", "use.covariates.if.available = TRUE")

# args= c("dataset=CMC", "condition=eRNA", "chromosome=21", "exprFileName=DLPFC.eRNA.KNOWN_AND_SVA.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv", "subset.snps.file=/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab", "subset.individuals.file=/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab", "geneFile=/sc/orga/projects/epigenAD/coloc/data/CMC/reference/eRNAgene.txt", "base.folder=/sc/orga/projects/epigenAD/coloc", "trans=FALSE",  "cisDist=1e6")

# --args dataset="CMC" condition="DLPFC" chromosome=chromosome exprFileName="DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv" subset.snps.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab" subset.individuals.file="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab" geneFile="/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt" base.folder="/sc/orga/projects/epigenAD/coloc" trans=TRUE cisDist=1e6
# dataset="CMC"; condition="DLPFC"; chromosome=21; start = 1; end = 300*10^6; pvOutputThreshold = 0; use.covariates.if.available = TRUE; base.folder="/sc/orga/projects/epigenAD/coloc"; exprFileName = "DLPFC.ensembl.KNOWN.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv"; min.MAF=0; geneFile = "/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt"; pvOutputThreshold_cis = 1; pvOutputThreshold_tra=1; cisDist = 1e6; subset.snps = "/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodSNPS.tab"; subset.individuals="/sc/orga/projects/epigenAD/coloc/data/CMC/support/goodIndividuals.tab"

# args= c('dataset=HBCC','condition=DLPFC','chromosome=22','exprFileName=DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.GE.WITHOUT_PRIMARY.RESIDUALS.tsv','subset.snps.file=NULL','subset.individuals.file=/sc/orga/projects/epigenAD/coloc/data/HBCC/support/HBCC_Caucasian_individuals','geneFile=/sc/orga/projects/epigenAD/coloc/data/CMC/reference/ensembl.genes.txt', 'base.folder=/sc/orga/projects/epigenAD/coloc','trans=FALSE', 'cisDist=1e6')
 
   # input.genotypes <- list.files(paste(base.folder, '/data/', dataset, '/rawData/genotypes',sep=''), pattern=genFileNamePattern, full.names=T)
   # message("Processing ", length(input.genotypes), " genotype files")

source("/hpc/users/giambc02/scripts/GENERAL/functions_QTL_pipeline.R")


prepare=TRUE
if (prepare) {

                    # this is the correct one: "DLPFC.ensembl.KNOWN_AND_SVA.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv",
                    # exprFileName = "DLPFC.ensembl.KNOWN.ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv",
                    prepare.matrixEQTL.pipeline(dataset=dataset, condition=condition, 
                       exprFileName = exprFileName,
                       use.covariates.if.available = TRUE,
                       chromosome=chromosome, start=1, end=300*10^6,
                       min.MAF = 0.03,
                       subset.snps.file =subset.snps.file,
                       subset.individuals.file=subset.individuals.file,
                       geneFile = geneFile,
                       base.folder="/sc/orga/projects/epigenAD/coloc")
}

run.eQTL.pipeline =TRUE
if (run.eQTL.pipeline) {
source("/hpc/users/giambc02/scripts/GENERAL/functions_QTL_pipeline.R")
                   eQTL.data = run.eQTL.pipeline(dataset=dataset, condition=condition, chromosome=chromosome, start= 1, end= 300*10^6, base.folder =base.folder, trans=FALSE, cisDist=cisDist)
}
# cisDist=1e6
# cisDist=2000
# cisDist=40000
#eQTL.data = run.eQTL(dataset=dataset, condition=condition, chromosome=chromosome,
#                     trans = trans,
#                     cisDist = cisDist,
#                     use.covariates.if.available = TRUE,
#                     base.folder = base.folder)
#}

runFromSummary = FALSE
if (runFromSummary) {

#chromosome, start, end,
#dataset, condition, base.folder = "/sc/orga/projects/epigenAD/coloc/"
#oFolder
                 base.folder = "/sc/orga/projects/epigenAD/coloc/"
                 message("Working with dataset ", dataset, " and condition ", condition)

                 region <- paste('chr', chromosome, '_', start, '_', end, sep = '')
                 if (start <= 1 && end >= 300*10^6) region <- paste('chr', chromosome, sep = '')
                 iFolder <- paste(base.folder, '/data/', dataset, '/eQTLs/matrixEQTL/', condition, sep = '')
                 input.eQTL.file <- paste(iFolder, '/', condition, '_', region, '_all_pval.tab', sep = '')

                 #condition = strsplit(dirname(input.eQTL.file), split="/")[[1]][[length(strsplit(dirname(input.eQTL.file), split="/")[[1]])]]
                 #dataset = strsplit(gsub(paste(base.folder, "data/", sep=""), "", input.eQTL.file), split="/")[[1]][1]
                 oFolder <- paste(base.folder, '/data/', dataset, '/eQTLs', sep = '')


                 runFromSummary(input.eQTL.file)
}




combine=FALSE
if (combine) {
   # eqtl_folder = "/sc/orga/projects/epigenAD/coloc/data/CMC/eQTLs/matrixEQTL/eRNA_OnlyGenes/"
   # eQTL.file = "/sc/orga/projects/epigenAD/coloc/data/CMC/eQTLs/matrixEQTL/eRNA_OnlyGenes/ALLsummary.tab"
   # eQTL.file.summary = "/sc/orga/projects/epigenAD/coloc/data/CMC/eQTLs/matrixEQTL/eRNA_OnlyGenes/summary.tab"
   eqtl_folder = "/sc/orga/projects/epigenAD/coloc/data/CMC/eQTLs/matrixEQTL/eRNA_NoGenes/"
   eQTL.file = paste(eqtl_folder, "ALLsummary.tab", sep="")
   eQTL.file.summary = paste(eqtl_folder, "summary.tab", sep="")
   source("/hpc/users/giambc02/scripts/GENERAL/functions_pipeline.R")
   combineCHR(base.folder=eqtl_folder, prefix="chr", suffix="_all_pval.tab", OutFile=eQTL.file) 
   source("/hpc/users/giambc02/scripts/GENERAL/functions_QTL_pipeline.R")
   res = summaryDF(eQTL.file, FDR=0.05)
   write.table(res, file=eQTL.file.summary, row.names = FALSE, quote = FALSE, col.names = TRUE, sep="\t")
}
